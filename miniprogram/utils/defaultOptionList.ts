import {
  PAI_QUARTER,
  PAI,
  PAI2,
  PAI3,
  PAI4,
  PAI5,
  PAI6,
  PAI_HALF,
  PAI_ONE_HALF,
} from "./constant";

const optionsList: IJiaPuPption[] = [
  { char: "5", level: 1, pai: PAI2 },
  { char: "5", level: 1, pai: PAI, split: true },

  { char: "5", level: 1, pai: PAI },
  { char: "3", level: 1, pai: PAI },
  { char: "5", level: 1, pai: PAI, split: true  },

  { char: "6", level: 1, pai: PAI2 },
  { char: "6", level: 1, pai: PAI , split: true},

  { char: "3", level: 1, pai: PAI3, split: true },
  { char: "1", level: 1, pai: PAI2 },
  { char: "1", level: 1, pai: PAI , split: true},
  { char: "2", level: 1, pai: PAI2 },
  { char: "5", level: 1, pai: PAI , split: true},

  { char: "3", level: 1, pai: PAI6 , split: true},
  { char: "5", level: 1, pai: PAI2 },
  { char: "5", level: 1, pai: PAI , split: true},

  { char: "5", level: 1, pai: PAI },
  { char: "3", level: 1, pai: PAI },
  { char: "5", level: 1, pai: PAI , split: true},

  { char: "6", level: 1, pai: PAI2 },
  { char: "6", level: 1, pai: PAI, split: true },

  { char: "3", level: 1, pai: PAI3, split: true },
  { char: "1", level: 1, pai: PAI2 },
  { char: "1", level: 1, pai: PAI , split: true},
  { char: "2", level: 1, pai: PAI },
  { char: "3", level: 1, pai: PAI },
  { char: "2", level: 1, pai: PAI , split: true},

  { char: "1", level: 1, pai: PAI6 , split: true},
  { char: "0", level: 1, pai: PAI },
  { char: "0", level: 1, pai: PAI },

  { char: "5", level: 0, pai: PAI , split: true},
  { char: "1", level: 1, pai: PAI3 , split: true},

  { char: "1", level: 1, pai: PAI },
  { char: "2", level: 1, pai: PAI },
  { char: "3", level: 1, pai: PAI , split: true},

  { char: "5", level: 1, pai: PAI },
  { char: "3", level: 1, pai: PAI },
  { char: "3", level: 1, pai: PAI3 },
  { char: "5", level: 0, pai: PAI , split: true},

  { char: "1", level: 1, pai: PAI3 , split: true},
  { char: "2", level: 1, pai: PAI },
  { char: "1", level: 1, pai: PAI },
  { char: "6", level: 0, pai: PAI , split: true},

  { char: "5", level: 0, pai: PAI5 },
  { char: "5", level: 0, pai: PAI , split: true},
  { char: "1", level: 1, pai: PAI3, split: true },
  { char: "1", level: 1, pai: PAI },
  { char: "2", level: 1, pai: PAI },
  { char: "3", level: 1, pai: PAI , split: true},

  { char: "6", level: 1, pai: PAI },
  { char: "3", level: 1, pai: PAI },
  { char: "5", level: 1, pai: PAI2 },
  { char: "3", level: 1, pai: PAI },
  { char: "2", level: 1, pai: PAI , split: true},

  { char: "1", level: 1, pai: PAI3 , split: true},
  { char: "2", level: 1, pai: PAI2 },
  { char: "5", level: 1, pai: PAI , split: true},
  { char: "3", level: 1, pai: PAI },
  { char: "2", level: 1, pai: PAI4 },
  { char: "3", level: 1, pai: PAI , split: true},

  { char: "5", level: 1, pai: PAI3 , split: true},
  { char: "5", level: 1, pai: PAI },
  { char: "3", level: 1, pai: PAI },
  { char: "5", level: 1, pai: PAI , split: true},
  { char: "6", level: 1, pai: PAI },
  { char: "1", level: 2, pai: PAI },
  { char: "6", level: 1, pai: PAI3 },

  { char: "3", level: 1, pai: PAI , split: true},
  { char: "2", level: 1, pai: PAI2 },
  { char: "1", level: 1, pai: PAI , split: true},
  { char: "2", level: 1, pai: PAI2 },
  { char: "3", level: 1, pai: PAI , split: true},
  { char: "3", level: 1, pai: PAI5 },

  { char: "5", level: 0, pai: PAI , split: true},
  { char: "5", level: 1, pai: PAI3 , split: true},
  { char: "0", level: 1, pai: PAI },
  { char: "3", level: 1, pai: PAI },
  { char: "3", level: 1, pai: PAI , split: true},

  { char: "2", level: 1, pai: PAI },
  { char: "1", level: 1, pai: PAI2 },
  { char: "1", level: 1, pai: PAI2 },
  { char: "5", level: 0, pai: PAI , split: true},

  { char: "2", level: 1, pai: PAI2 },
  { char: "1", level: 1, pai: PAI , split: true},
  { char: "3", level: 1, pai: PAI },
  { char: "2", level: 1, pai: PAI },
  { char: "1", level: 1, pai: PAI, split: true },

  { char: "1", level: 1, pai: PAI6, split: true },
  { char: "0", level: 1, pai: PAI },
  { char: "0", level: 1, pai: PAI },
  { char: "5", level: 0, pai: PAI, split: true },

  { char: "1", level: 1, pai: PAI3 , split: true},
  { char: "1", level: 1, pai: PAI },
  { char: "2", level: 1, pai: PAI },
  { char: "3", level: 1, pai: PAI, split: true },

  { char: "5", level: 1, pai: PAI },
  { char: "3", level: 1, pai: PAI },
  { char: "3", level: 1, pai: PAI3 },
  { char: "5", level: 0, pai: PAI , split: true},

  { char: "1", level: 1, pai: PAI2 },
  { char: "1", level: 1, pai: PAI , split: true},
  { char: "2", level: 1, pai: PAI },
  { char: "1", level: 1, pai: PAI },
  { char: "6", level: 0, pai: PAI , split: true},

  { char: "5", level: 0, pai: PAI5 },
  { char: "5", level: 0, pai: PAI , split: true},
  { char: "1", level: 1, pai: PAI3, split: true },

  { char: "1", level: 1, pai: PAI },
  { char: "2", level: 1, pai: PAI },
  { char: "3", level: 1, pai: PAI , split: true},
  { char: "6", level: 1, pai: PAI },
  { char: "3", level: 1, pai: PAI },
  { char: "5", level: 1, pai: PAI2 },

  { char: "3", level: 1, pai: PAI },
  { char: "2", level: 1, pai: PAI , split: true},
  { char: "1", level: 1, pai: PAI3 , split: true},

  { char: "2", level: 1, pai: PAI2 },
  { char: "5", level: 1, pai: PAI , split: true},
  { char: "3", level: 1, pai: PAI },
  { char: "2", level: 1, pai: PAI2 },
  { char: "2", level: 1, pai: PAI2 },
  { char: "3", level: 1, pai: PAI , split: true},

  { char: "5", level: 1, pai: PAI2 },
  { char: "5", level: 1, pai: PAI, split: true },
  { char: "5", level: 1, pai: PAI },
  { char: "3", level: 1, pai: PAI },
  { char: "5", level: 1, pai: PAI , split: true},

  { char: "6", level: 1, pai: PAI },
  { char: "1", level: 2, pai: PAI },
  { char: "6", level: 1, pai: PAI3 },
  { char: "3", level: 1, pai: PAI , split: true},

  { char: "2", level: 1, pai: PAI2 },
  { char: "1", level: 1, pai: PAI, split: true },
  { char: "2", level: 1, pai: PAI2 },
  { char: "3", level: 1, pai: PAI , split: true},

  { char: "3", level: 1, pai: PAI5 },
  { char: "3", level: 1, pai: PAI , split: true},
  { char: "5", level: 1, pai: PAI3 , split: true},

  { char: "0", level: 1, pai: PAI },
  { char: "0", level: 1, pai: PAI },
  { char: "3", level: 1, pai: PAI_HALF },
  { char: "2", level: 1, pai: PAI_HALF , split: true},

  { char: "6", level: 0, pai: PAI },
  { char: "1", level: 1, pai: PAI2 , split: true},
  { char: "1", level: 1, pai: PAI2 },
  { char: "2", level: 1, pai: PAI , split: true},

  { char: "1", level: 1, pai: PAI6 , split: true},
  { char: "0", level: 1, pai: PAI },
  { char: "0", level: 1, pai: PAI },
  { char: "3", level: 1, pai: PAI, split: true },
  { char: "5", level: 1, pai: PAI2 },
  { char: "5", level: 1, pai: PAI , split: true},

  { char: "5", level: 1, pai: PAI },
  { char: "3", level: 1, pai: PAI },
  { char: "5", level: 1, pai: PAI , split: true},
  { char: "6", level: 1, pai: PAI2 },
  { char: "6", level: 1, pai: PAI , split: true},

  { char: "3", level: 1, pai: PAI2 },
  { char: "2", level: 1, pai: PAI , split: true},
  { char: "1", level: 1, pai: PAI3 , split: true},
  { char: "2", level: 1, pai: PAI2 },
  { char: "5", level: 1, pai: PAI , split: true},

  { char: "3", level: 1, pai: PAI3 , split: true},
  { char: "0", level: 1, pai: PAI },
  { char: "0", level: 1, pai: PAI },
  { char: "3", level: 1, pai: PAI, split: true },
  { char: "5", level: 1, pai: PAI3 , split: true},

  { char: "5", level: 1, pai: PAI },
  { char: "3", level: 1, pai: PAI },
  { char: "5", level: 1, pai: PAI , split: true},

  { char: "6", level: 1, pai: PAI2 },
  { char: "6", level: 1, pai: PAI , split: true},
  { char: "3", level: 1, pai: PAI2 },
  { char: "2", level: 1, pai: PAI , split: true},
  { char: "1", level: 1, pai: PAI2 },
  { char: "1", level: 1, pai: PAI , split: true},

  { char: "2", level: 1, pai: PAI2 },
  { char: "3", level: 1, pai: PAI , split: true},
  { char: "1", level: 1, pai: PAI6 , split: true},

  { char: "0", level: 1, pai: PAI },
  { char: "0", level: 1, pai: PAI },
  { char: "6", level: 0, pai: PAI_HALF },
  { char: "6", level: 0, pai: PAI_HALF , split: true},

  { char: "1", level: 1, pai: PAI2 },
  { char: "1", level: 1, pai: PAI , split: true},
  { char: "2", level: 1, pai: PAI },
  { char: "2", level: 1, pai: PAI },
  { char: "5", level: 1, pai: PAI , split: true},

  { char: "3", level: 1, pai: PAI5 },
  { char: "3", level: 1, pai: PAI_HALF },
  { char: "5", level: 1, pai: PAI_HALF, split: true },

  { char: "6", level: 1, pai: PAI2 },
  { char: "5", level: 1, pai: PAI, split: true },
  { char: "6", level: 1, pai: PAI },
  { char: "5", level: 1, pai: PAI },
  { char: "2", level: 1, pai: PAI, split: true },

  { char: "3", level: 1, pai: PAI5 },
  { char: "1", level: 1, pai: PAI , split: true},
  { char: "2", level: 1, pai: PAI2 },
  { char: "1", level: 1, pai: PAI , split: true},

  { char: "2", level: 1, pai: PAI2 },
  { char: "6", level: 0, pai: PAI , split: true},
  { char: "2", level: 1, pai: PAI2 },
  { char: "3", level: 1, pai: PAI , split: true},

  { char: "2", level: 1, pai: PAI2 },
  { char: "1", level: 1, pai: PAI , split: true},
  { char: "2", level: 1, pai: PAI },
  { char: "2", level: 1, pai: PAI },
  { char: "2", level: 1, pai: PAI , split: true},

  { char: "2", level: 1, pai: PAI },
  { char: "3", level: 1, pai: PAI },
  { char: "6", level: 1, pai: PAI , split: true},
  { char: "5", level: 1, pai: PAI6 , split: true},

  { char: "0", level: 1, pai: PAI },
  { char: "3", level: 1, pai: PAI },
  { char: "5", level: 1, pai: PAI , split: true},
  { char: "5", level: 1, pai: PAI2 },
  { char: "5", level: 1, pai: PAI , split: true},

  { char: "5", level: 1, pai: PAI },
  { char: "3", level: 1, pai: PAI },
  { char: "5", level: 1, pai: PAI , split: true},
  { char: "6", level: 1, pai: PAI2 },
  { char: "3", level: 1, pai: PAI_HALF },
  { char: "2", level: 1, pai: PAI_HALF, split: true },

  { char: "3", level: 1, pai: PAI },
  { char: "2", level: 1, pai: PAI },
  { char: "1", level: 1, pai: PAI , split: true},
  { char: "1", level: 1, pai: PAI2 },
  { char: "1", level: 1, pai: PAI , split: true},

  { char: "2", level: 1, pai: PAI2 },
  { char: "5", level: 1, pai: PAI , split: true},
  { char: "3", level: 1, pai: PAI6 , split: true},

  { char: "5", level: 1, pai: PAI2 },
  { char: "5", level: 1, pai: PAI , split: true},
  { char: "5", level: 1, pai: PAI },
  { char: "3", level: 1, pai: PAI },
  { char: "5", level: 1, pai: PAI, split: true },

  { char: "6", level: 1, pai: PAI2 },
  { char: "6", level: 1, pai: PAI , split: true},
  { char: "3", level: 1, pai: PAI },
  { char: "2", level: 1, pai: PAI },
  { char: "1", level: 1, pai: PAI , split: true},
];

export default optionsList;
