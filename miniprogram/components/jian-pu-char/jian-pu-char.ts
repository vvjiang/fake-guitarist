import {
  PAI2,
  PAI3,
  PAI4,
  PAI5,
  PAI6,
  PAI_ONE_HALF,
} from "../../utils/constant";

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    // 这里定义了 innerText 属性，属性值可以在组件使用时指定
    info: {
      type: Object,
    },
    active: {
      type: Boolean,
    },
    editting: {
      type: Boolean,
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    container: "",
  },

  /**
   * 组件的方法列表
   */
  methods: {},
  lifetimes: {
    attached: function () {},
    detached: function () {
      // 在组件实例被从页面节点树移除时执行
    },
  },
  observers: {
    "info, active, editting": function (info, active, editting) {
      if (!info) {
        return;
      }
      let className = "";
      if (info.pai === PAI6) {
        className = "jianpu-char jianpu-char-6";
      } else if (info.pai === PAI5) {
        className = "jianpu-char jianpu-char-5";
      } else if (info.pai === PAI4) {
        className = "jianpu-char jianpu-char-4";
      } else if (info.pai === PAI3) {
        className = "jianpu-char jianpu-char-3";
      } else if (info.pai === PAI2 || info.pai === PAI_ONE_HALF) {
        className = "jianpu-char jianpu-char-2";
      } else {
        className = "jianpu-char";
      }
      if (info.split) {
        className += " jianpu-char-split";
      }
      if (active) {
        className += " jianpu-char-active";
      }
      if (editting) {
        className += " jianpu-char-active-editting";
      }
      this.setData({
        container: className,
      });
    },
  },
});
